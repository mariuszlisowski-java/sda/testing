package pl.sdacademy;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {
    boolean isEmailVaild(String email) {
        String emailRegex = "^([\\w-.]+){2,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }
}

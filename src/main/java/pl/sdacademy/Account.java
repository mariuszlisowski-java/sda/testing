package pl.sdacademy;

public class Account {
    protected int balance;
    private final int ACCOUNT_NO_LENGHT = 26;
        String accountNo;
    private String name;
    private String surname;

    public Account(String accountNo) {
        this.accountNo = accountNo;
    }

    public boolean transferFunds(Account recipientAccount, int amountToTranfer) {
        if (recipientAccount.isAccountNumberValid() &&
            this.isEnoughFunds(amountToTranfer))
        {
            this.balance -= amountToTranfer;
            recipientAccount.balance += amountToTranfer;
            return true;
        }

        return false;
    }

    protected boolean isAccountNumberValid() {
        return accountNo.length() == ACCOUNT_NO_LENGHT;
    }

    protected boolean isEnoughFunds(int amountToTranfer) {
        return balance >= amountToTranfer;
    }
}

package pl.sdacademy;

import java.io.*;

public class FileOperations {
    protected DataOutputStream dataOut;
    protected DataInputStream dataIn;

    public boolean openFileForWriting(String filename) throws IOException {
        try {
            dataOut = new DataOutputStream(new FileOutputStream(filename));
            System.out.println("File open for writing success!");
            return true;
        } catch (IOException ex) {
            System.out.println("File open for writing error!");
            return false;
        }
    }

    public boolean writeToFile(int dataToWrite) throws IOException {
        try {
            if (dataOut != null) {
                dataOut.writeInt(dataToWrite);
                return true;
            } else {
                return false;
            }
        } catch (IOException ex) {
            System.out.println("File write error!");
            return false;
        }
    }

    public boolean openFileForReading(String filename) throws IOException {
        try {
            dataIn = new DataInputStream(new FileInputStream(filename));
            System.out.println("File open for reading!");
            return true;
        } catch (IOException ex) {
            System.out.println("File open for reading error!");
            return false;
        }
    }

    public boolean readFromFile(String filename) {
        try {
            if (dataOut != null) {
                int dataRead = dataIn.readInt();
                System.out.println("Data read: " + dataRead);
                return true;
            } else {
                return false;
            }
        } catch (IOException ex) {
            System.out.println("File read error!");
            return false;
        }
    }

}

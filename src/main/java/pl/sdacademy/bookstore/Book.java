package pl.sdacademy.bookstore;

public class Book {
    private String title;
    private String author;
    protected int price;

    public Book(String title, String author, int price) {
        this.title = title;
        this.author = author;
        this.price = price;
    }

}

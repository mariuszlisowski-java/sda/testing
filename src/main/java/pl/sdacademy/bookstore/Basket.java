package pl.sdacademy.bookstore;

import java.util.ArrayList;
import java.util.List;

public class Basket {
    protected final List<Book> books;

    public Basket() {
        books = new ArrayList<>();
    }

    public void addToBasket(Book book) {
        books.add(book);
    }

    public int valueOfbasket() {
        int value = 0;
        System.out.println("Size: " + numberOfBooksInBasket());
        for (Book book : books) {
            System.out.println(book.price);
            value += book.price;
        }

        return value;
    }

    public void clearBasket() {
        books.clear();
    }

    protected boolean doesContainsBook(Book book) {
        return books.contains(book);
    }

    protected int numberOfBooksInBasket() {
        return books.size();
    }

}

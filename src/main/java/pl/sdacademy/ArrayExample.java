package pl.sdacademy;

import java.util.Arrays;
import java.util.LinkedHashSet;

public class ArrayExample {
    public static String[] removeDuplicatesOrdered(String[] array) {
        return new LinkedHashSet<String>(Arrays.asList(array)).toArray(String[]::new);
    }

}

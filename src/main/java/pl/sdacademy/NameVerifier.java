package pl.sdacademy;

public class NameVerifier {

    public boolean isValidString(String str) {
        return str != null &&
               !str.equals("") &&
               !str.toLowerCase().equals(str) &&
               !str.toUpperCase().equals(str);
    }

}

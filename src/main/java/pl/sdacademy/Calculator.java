package pl.sdacademy;

import java.security.InvalidParameterException;

public class Calculator {
    public double add(double a, double b) {
        return a + b;
    }

    public double subtract(double a, double b) {
        return a - b;
    }

    public double multiply(double a, double b) {
        return a * b;
    }

    public double divideExcArithm(double a, double b) {
        if (b != 0) {
            return a / b;
        } else {
            throw new ArithmeticException("Do NOT divide by zero!");
        }
    }

    public double divideExcParam(double a, double b) {
        if (b != 0) {
            return a / b;
        } else {
            throw new InvalidParameterException("Divide by zero!");
        }
    }

}

package pl.sdacademy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {
    Account senderAccount = new Account("12345678912345678912345678");
    Account recipientAccount = new Account("12345678912345678912345671");
    @BeforeEach

    @Test
    void shouldFailForLenghtOfAccount() {
        // given
        senderAccount.accountNo = "1234567891234567891234567"; // 25
        // when
        boolean isAccountValid = senderAccount.isAccountNumberValid();
        // then
        assertFalse(isAccountValid);
    }

    @Test
    void shouldPassForLenghtOfAccount() {
        // given
        senderAccount.accountNo = "12345678912345678912345678"; // 26
        // when
        boolean isAccountValid = senderAccount.isAccountNumberValid();
        // then
        assertTrue(isAccountValid);
    }

    @Test
    void shouldFailForNoEnoughFunds() {
        // given
        int amountToTranfer = 101;
        senderAccount.balance = 100;
        // when
        boolean isEnoughFunds = senderAccount.isEnoughFunds(amountToTranfer);
        // then
        assertFalse(isEnoughFunds);
    }

    @Test
    void shouldPassForEnoughFunds() {
        // given
        int amountToTranfer = 100;
        senderAccount.balance = 100;
        // when
        boolean isEnoughFunds = senderAccount.isEnoughFunds(amountToTranfer);
        // then
        assertTrue(isEnoughFunds);
    }

    @Test
    void shoudTransferAmountToRecipientAccount() {
        // given
        senderAccount.balance = 100;
        int amountToTransfer = 100;
        // when
        boolean isSuccess = senderAccount.transferFunds(recipientAccount, amountToTransfer);
        // then
        assertTrue(isSuccess);
    }

    @Test
    void shoudNotTransferAmountToRecipientAccount() {
        // given
        senderAccount.balance = 0;
        int amountToTransfer = 100;
        // when
        boolean isSuccess = senderAccount.transferFunds(recipientAccount, amountToTransfer);
        // then
        assertFalse(isSuccess);
    }

    @Test
    void shouldTransferAmountToRecipientAccountReceipientBalance() {
        // given
        senderAccount.balance = 100;
        int amountToTransfer = 100;
        // when
        int previousRecipientBalance = recipientAccount.balance;
        senderAccount.transferFunds(recipientAccount, amountToTransfer);
        int currentRecipientBalance = recipientAccount.balance;
        // then
        assertEquals(previousRecipientBalance + amountToTransfer, currentRecipientBalance);
    }

    @Test
    void shouldTransferAmountToRecipientAccountSenderBalance() {
        // given
        senderAccount.balance = 101;
        int amountToTransfer = 100;
        // when
        int previousSenderBalance = senderAccount.balance;
        senderAccount.transferFunds(recipientAccount, amountToTransfer);
        int currentSenderBalance = senderAccount.balance;
        // then
        assertEquals(previousSenderBalance - amountToTransfer, currentSenderBalance);
    }

    @Test
    void shouldNotTransferForNotEnoughFundsReceipientBalance() {
        // given
        senderAccount.balance = 99;
        int amountToTransfer = 100;
        // when
        int previousRecipientBalance = recipientAccount.balance;
        senderAccount.transferFunds(recipientAccount, amountToTransfer);
        int currentRecipientBalance = recipientAccount.balance;
        // then
        assertEquals(previousRecipientBalance, currentRecipientBalance);
    }

    @Test
    void shouldNotTransferForNotEnoughFundsSenderBalance() {
        // given
        senderAccount.balance = 99;
        int amountToTransfer = 100;
        // when
        int previousSenderBalance = senderAccount.balance;
        senderAccount.transferFunds(recipientAccount, amountToTransfer);
        int currentSenderBalance = senderAccount.balance;
        // then
        assertEquals(previousSenderBalance, currentSenderBalance);
    }

}
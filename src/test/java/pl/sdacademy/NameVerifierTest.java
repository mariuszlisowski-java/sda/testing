package pl.sdacademy;

// JUnit
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
// AssertJ
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

import static org.junit.jupiter.api.Assertions.*;

class NameVerifierTest {
    NameVerifier nameVerifier = new NameVerifier();
    @BeforeEach

    @Test
    public void shouldNotBeNull() {
        String nullString = null;
        boolean validationResult = nameVerifier.isValidString(nullString);
//        assertFalse(validationResult);
        assertThat(validationResult).isFalse();
    }

    @Test
    public void shouldBeProperString() {
        String properString = "Aa";
        boolean validationResult = nameVerifier.isValidString(properString);
        assertThat(validationResult).isTrue();
    }

    @Test
    public void shouldNotBeEmpty() {
        String emptyString = "";
        boolean validationResult = nameVerifier.isValidString(emptyString);
        assertThat(validationResult).isNotNull().isFalse();
    }

    @Test
    public void shouldReturnFalseWhenNoLowercaseLetterString() {
        String lowercaseString = "ala";
        boolean validationResult = nameVerifier.isValidString(lowercaseString);

        assertFalse(validationResult);
    }

    @Test
    public void shouldReturnFalseWhenNoUppercaseLetterString() {
        String uppercaseString = "ALA";
        boolean validationResult = nameVerifier.isValidString(uppercaseString);

        assertFalse(validationResult);
    }

}

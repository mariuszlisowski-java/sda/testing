package pl.sdacademy;

import java.io.IOException;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class FileOperationsTest {
    private static FileOperations fileOperations;

    @BeforeAll
    public static void init() {
        fileOperations = new FileOperations();
        System.out.println("Initialization... object created!");
    }
    @AfterAll
    public static void exit() throws IOException {
        fileOperations.dataOut.close();
        System.out.println("Exitting... file closed!");
    }

    @Test
    @DisplayName("#1. File open to write")
    void shouldOpenFileForWriting() throws IOException {
        // given
        String filename = "test.file";
        // when
        boolean isFileOpen = fileOperations.openFileForWriting(filename);
        // then
        assertTrue(isFileOpen);
    }

    @Test
    @DisplayName("#2. File write")
    void shoudWriteToFile() throws IOException {
        // given
        String filename = "test.file";
        int dataToWrite = 4096;
        // when
        fileOperations.openFileForWriting(filename);
        boolean isWrittenToFile = fileOperations.writeToFile(dataToWrite);
        // then
        assertTrue(isWrittenToFile);
    }

    @Test
    @DisplayName("#3. File open to read")
    void shouldOpenFileForReading() throws IOException {
        // given
        String filename = "test.file";
        int dataToWrite = 4096;
        // when
        fileOperations.openFileForWriting(filename);
        boolean isFileOpen = fileOperations.openFileForReading(filename);
        // then
        assertTrue(isFileOpen);
    }

    @Test
    @DisplayName("#4. File read")
    void shouldReadFromFile() throws IOException {
        // given
        String filename = "test.file";
        int dataToWrite = 4096;
        // when
        fileOperations.openFileForWriting(filename);
        fileOperations.writeToFile(dataToWrite);
        fileOperations.openFileForReading(filename);
        boolean isReadFromFile = fileOperations.readFromFile(filename);
        // then
        assertTrue(isReadFromFile);
    }

}
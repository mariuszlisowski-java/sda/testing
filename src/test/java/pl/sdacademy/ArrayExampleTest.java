package pl.sdacademy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayExampleTest {
    @Test
    void shouldRemoveDuplicatesFromDuplicatedArray() {
        // given
        String[] input = {"ola", "nie", "ala", "ma", "ala", "kota", "burka", "burka", "ola"};
        String[] expected = {"ola", "nie", "ala", "ma", "kota", "burka"};
        // when
        String[] actual = ArrayExample.removeDuplicatesOrdered(input);
        // then
        assertArrayEquals(expected,actual);
    }

    @Test
    void shouldNotRemoveAnythingAsNoDuplicatedPassed() {
        // given
        String[] input = {"ola", "nie", "ala", "ma", "kota", "burka"};
        String[] expected = {"ola", "nie", "ala", "ma", "kota", "burka"};
        // when
        String[] actual = ArrayExample.removeDuplicatesOrdered(input);
        // then
        assertArrayEquals(expected,actual);
    }

}
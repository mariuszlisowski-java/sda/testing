package pl.sdacademy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class EmailValidatorTest {
    EmailValidator emailValidator = new EmailValidator();
    @BeforeEach

    @Test
    void shouldFailForNoUsername() {
        String email = "@cd.ef";
        assertFalse(emailValidator.isEmailVaild(email));
    }

    @Test
    void shouldFailForOneLetterInUsername() {
        String email = "a@cd.ef";
        assertFalse(emailValidator.isEmailVaild(email));
    }

    @Test
    void shouldFailForNoHostname() {
        String email = "ab@.ef";
        assertFalse(emailValidator.isEmailVaild(email));
    }

    @Test
    void shouldFailForOneLetterInHostname() {
        String email = "ab@c.ef";
        assertFalse(emailValidator.isEmailVaild(email));
    }

    @Test
    void shouldFailForNoDomain() {
        String email = "ab@cd.";
        assertFalse(emailValidator.isEmailVaild(email));
    }

    @Test
    void shouldFailForOneLetterInDomain() {
        String email = "ab@cd.e";
        assertFalse(emailValidator.isEmailVaild(email));
    }

    @Test
    void shouldFailForDotInDomain() {
        String email = "ab@c.d.e";
        assertFalse(emailValidator.isEmailVaild(email));
    }

    @Test
    void shouldPassForCompleteAddress() {
        String email = "ab@cd.ef";
        assertTrue(emailValidator.isEmailVaild(email));
    }

    @Test
    void shouldPassForCompleteAddressWithDotInUsername() {
        String email = "ab.cd@ef.gh";
        assertTrue(emailValidator.isEmailVaild(email));
    }

}

package pl.sdacademy;

// JUnit
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
// AssertJ
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

import java.security.InvalidParameterException;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    Calculator calc = new Calculator();
    @BeforeEach

    @Test
    public void shouldAddTwoDoubles() {
        // given
        double a = -3.5;
        double b = 1.5;

        // when
        double result = calc.add(a, b);

        // then
        assertEquals(-2.0, result);
    }

    @Test
    public void shouldSubtractTwoDoubles() {
        // given
        double a = -3.5;
        double b = 1.5;
        // when
        double result = calc.subtract(a, b);
        // then
        assertEquals(-5.0, result);
    }

    @Test
    public void shouldMultiplyTwoDoubles() {
        // given
        double a = -3.5;
        double b = 0;
        // when
        double result = calc.multiply(a, b);
        // then
        assertEquals(0, result, 0.001);
    }

    @Test
    public void shouldThrowArithmeticExeptionWhenDivideByZero() {
        // given
        double a = -3.5;
        double b = 0;
        // when
        // ....
        // then
        assertThrows(ArithmeticException.class, () -> {
            double result = calc.divideExcArithm(a, b);
        });
    }

    @Test
    public void shouldThrowParameterExeptionWhenDivideByZero() {
        // given
        double a = -3.5;
        double b = 0;
        // when
        Executable result = () -> { calc.divideExcParam(a, b); };
        // then
        assertThrows(InvalidParameterException.class, result);
    }

    // AssertJ
    @Test
    public void shoudAddTwoDoublesJ() {
        // given
        double a = -3.5;
        double b = 1.5;
        // when
        double result = calc.add(a, b);
        // then
        assertThat(result).isEqualTo(-2.0);
    }

    @Test
    public void shoudSubtractTwoDoublesJ() {
        // given
        double a = -3.5;
        double b = 1.5;
        // when
        double result = calc.subtract(a, b);
        // then
        assertThat(result).isEqualTo(-5.0);
    }

    @Test
    public void shoudMultiplyTwoDoublesJ() {
        // given
        double a = -3.5;
        double b = 0;
        // when
        double result = calc.multiply(a, b);
        // then
        assertThat(result).isEqualTo(0);
    }

    @Test
    public void shoudDivideTwoDoublesJ() {
        // given
        double a = -3.5;
        double b = 0;
        // when
        // ..
        // then
        assertThatThrownBy(() -> { calc.divideExcArithm(a, b); }).isInstanceOf(ArithmeticException.class);
    }

}

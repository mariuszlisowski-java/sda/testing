package pl.sdacademy.bookstore;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class BasketTest {
    @Test
    void shouldAddBookToBasket() {
        // given
        Basket basket = new Basket();
        Book book = new Book("Saw", "Hawks", 9);
        // when
        basket.addToBasket(book);
        // then
        assertTrue(basket.doesContainsBook(book));
    }

    @Test
    void shouldAddFewBooksToBasket() {
        // given
        Basket basket = new Basket();
        Book bookA = new Book("Hammer", "Wats", 5);
        Book bookB = new Book("Chain", "Moore", 6);
        // when
        basket.addToBasket(bookA);
        basket.addToBasket(bookB);
        // then
        assertEquals(2, basket.numberOfBooksInBasket());
    }

    @Test
    void shouldSumValueOfBasket() {
        // given
        Basket basket = new Basket();
        Book bookA = new Book("Hammer", "Wats", 5);
        Book bookB = new Book("Chain", "Moore", 6);
        // when
        basket.addToBasket(bookA);
        basket.addToBasket(bookB);
        int valueOfBasket = basket.valueOfbasket();
        // then
        assertEquals(11, valueOfBasket);
    }

    @Test
    void shouldClearBasket() {
        // given
        Basket basket = new Basket();
        Book book = new Book("Saw", "Hawks", 9);
        // when
        basket.addToBasket(book);
        basket.clearBasket();
        // then
        assertEquals(0, basket.numberOfBooksInBasket());
    }
}